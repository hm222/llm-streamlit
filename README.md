# Streamlit App Name

Hugging Face LLM Chatbot

## Features

- Interaction with GPT-2 model from Hugging Face (tried with better-performed models on Hugging Face but that version cannot be deployed to streamlit cloud due to resource limitations).
- Manually defined CSS styles and inclusion of background images with caption.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

```bash
Python 3.11
pip
```

### Installing

Clone the repository:

```bash
git clone https://gitlab.com/hm222/llm-streamlit
```

Navigate to the project directory:

```bash
cd llm-streamlit
```

Install the required packages:

```bash
pip install -r requirements.txt
```

Run the app:

```bash
streamlit run app.py
```

## Using the App

You can either run the app locally following the instructions above, or go to [this link](https://llm-app-ri2rnjvsm7byhjonqowkto.streamlit.app/) to interact with the bot.

## Deployment

The ChatBot is deployed to [streamlit](https://llm-app-ri2rnjvsm7byhjonqowkto.streamlit.app/).

## Built With

- [Streamlit](https://streamlit.io)
- [GPT2 Model](https://huggingface.co/openai-community/gpt2)
- Also tried with this model locally: [Austism/chronos-hermes-13b-v2](https://huggingface.co/Austism/chronos-hermes-13b-v2).